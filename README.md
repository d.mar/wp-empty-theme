# Wordpress empty theme

Bare minimal wordpress theme.

<!-- LICENSE -->
## License

Distributed under the Public domain license. See `LICENSE` for more information.

<!-- CONTACT -->
## Contact

David Martín - hola@dmar.dev - [https://dmar.dev](https://dmar.dev/) 

Project Link: [https://gitlab.com/d.mar/wp-empty-theme](https://gitlab.com/d.mar/wp-empty-theme)
